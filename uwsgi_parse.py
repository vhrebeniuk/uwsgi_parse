#!/usr/bin/python

import re, psycopg2, os, getpass, paramiko
from datetime import datetime
from itertools import chain


class Database:

    def __init__(self, db_name='parse_log', db_passwd="mHoPro45", db_host="localhost", db_user="postgres"):
        self.db_host = db_host
        self.db_user = db_user
        self.db_passwd = db_passwd
        self.db_name = db_name

    def __enter__(self):
        try:
            self.connection = psycopg2.connect(host=self.db_host,
                                               user=self.db_user,
                                               password=self.db_passwd,
                                               dbname=self.db_name)
            self.cursor = self.connection.cursor()
            return self.cursor
        except psycopg2.Error as e:
            print "Error: {}".format(e)
            self.__exit__(e)

    def __exit__(self, *args):
        self.connection.close()

    def add_project(self, project, page, uwsgi_way, ip, port):
        """add new project. required: project name, last page,
        path to uwsgi log, ip address of the server and port"""

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS projects(id SERIAL PRIMARY KEY, project TEXT UNIQUE, page INT, uwsgi_way TEXT, ip TEXT, port INT)
        ''')
        query = """INSERT INTO projects(project, page, uwsgi_way, ip, port) VALUES (%s, %s, %s, %s, %s)"""
        self.cursor.execute(query, (project, page, uwsgi_way, ip, port))
        self.connection.commit()


    def create_uwsgi_log_table(self):
        self.cursor.execute('''
                    CREATE TABLE IF NOT EXISTS uwsgi_log(ORDER_ID SERIAL PRIMARY KEY, project_id integer REFERENCES projects,url_path TEXT, method TEXT, status TEXT, resp_size INT, resp_time INT, request_datetime timestamp)
                ''')
        self.connection.commit()

    def update_project_page(self, page, project):
        query = """Update projects set page = %s where project like %s"""
        self.cursor.execute(query, (page, project))
        self.connection.commit()

    def get_init_data(self, project):
        """return last page, path to uwsgi log, ip address of the server, port"""
        query = "select * from projects where project like %s"
        self.cursor.execute(query, (project,))
        count = self.cursor.fetchall()
        count = count[0]
        self.page, self.path, self.ip, self.port, self.project_id= count[2], count[3], count[4], count[5], count[0]

    def get_parse_data(self, project):
        query = "select * from uwsgi_log join projects on uwsgi_log.project_id = projects.id where projects.project = %s"
        self.cursor.execute(query, (project,))
        return self.cursor.fetchall()

    def save_parse_result(self, results):
        values = ",".join("(%s, %s, %s, %s, %s, %s, %s)" for item in results)
        query = "INSERT INTO uwsgi_log(url_path, method, status, resp_size, resp_time, request_datetime, project_id) VALUES {}".format(values)
        self.cursor.execute(query, list(chain.from_iterable(results)))
        self.connection.commit()


class ParseUwsgi:

    def __init__(self, project):
        self.project = project

    def parse_line(self, line):
        DATETIME_FORMAT = '%a %b %d %H:%M:%S %Y'
        patt = re.compile(r'''}\ \[(?P<datetime>.*?)\]\ (?P<request_method>POST|GET|DELETE|PUT|PATCH)\s
            (?P<request_uri>[^ ]*?)\ =>\ generated\ (?P<resp_size>.*)\ bytes\ in\ (?P<resp_msecs>\d+)\ msecs\s
            \(HTTP/[\d.]+\ (?P<resp_status>\d+)\)''', re.VERBOSE)
        matched = patt.search(line)
        if matched:
            matched_dict = matched.groupdict()
            method = matched_dict['request_method']
            status = matched_dict['resp_status']
            resp_size = matched_dict['resp_size']
            url = matched_dict['request_uri'].replace('//', '/')
            url_path = url.split('?')[0]
            resp_time = int(matched_dict['resp_msecs'])
            request_datetime = datetime.strptime(matched_dict['datetime'],
                                                          DATETIME_FORMAT)

            return url_path, method, status,  resp_size, resp_time, request_datetime, self.db.project_id

    def parser(self, lines):
        results = []
        try:
            for line in lines:
                check = self.parse_line(line)
                if check:
                    results.append(check)
            return results
        except Exception as e:
            print "Error: {}".format(e)

    def parse_log(self):
        last_page = 0
        username = raw_input("Username: ")
        passwd = getpass.getpass()
        self.db = Database()
        with self.db:
            self.db.create_uwsgi_log_table()
            self.db.get_init_data(self.project)
            with SshToUwsgi(ssh_host=self.db.ip, ssh_name=username,
                       ssh_passwd=passwd, ssh_port=self.db.port,
                       path=self.db.path) as uwsgi_log:
                results = []
                for counter, line in enumerate(uwsgi_log):
                    if counter > self.db.page:
                        check = self.parse_line(line)
                        if check:
                            results.append(check)
                        if len(results) == 1000:
                            self.db.save_parse_result(results)
                            results = []
                    last_page = counter
                self.db.save_parse_result(results)
                self.db.update_project_page(last_page, self.project)


class SshToUwsgi:

    def __init__(self, ssh_name, ssh_passwd, path, ssh_port=22, ssh_host="127.0.0.1"):
        self.ssh_host = ssh_host
        self.ssh_passwd = ssh_passwd
        self.ssh_port = ssh_port
        self.ssh_name = ssh_name
        self.path = path
        self.local_path= '/tmp/uwsgi.log'

    def __enter__(self):
        try:
            self.client = paramiko.SSHClient()
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.connect(hostname=self.ssh_host, username=self.ssh_name, password=self.ssh_passwd, port=self.ssh_port)
            self.sftp_client = self.client.open_sftp()
            print self.path + ' >>> ' + self.local_path
            self.local_file = self.sftp_client.get(self.path, self.local_path)

            self.open_file = open(self.local_path)
            return self.open_file
        except paramiko.SSHException as e:
            print "Error: {}".format(e)
            self.__exit__(e)
        except paramiko.AUTH_FAILED as e:
            print "Authentication error: {}".format(e)
            self.__exit__(e)

    def __exit__(self, *args):
        self.open_file.close()
        self.sftp_client.close()
        self.client.close()
        os.remove(self.local_path)




if __name__ == '__main__':

    def bs():
        ps = ParseUwsgi('c24')
        ps.parse_log()
    bs()
    # with PyCallGraph(output=GraphvizOutput()):
    #     bs()



